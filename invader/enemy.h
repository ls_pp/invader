//
//  enemy.h
//  invader
//
//  Created by Choe Yong-uk on 12. 2. 14..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Enemy : NSObject
{
  CCSprite *sprite; 
  CGPoint position; // 현재 픽셀 상대(ccp)좌표
  int xx;
  int yy; // 맵에서 어디에 위치하고 있는지 
  float moveSpeed;
  
  //BOOL _left1st;  // 제일 왼쪽인가?
  //BOOL _right1st;  // 제일 오른쪽인가 ?
  //BOOL directionRightTo; // 오른쪽으로 이동 중이면 YES 왼쪽이면 NO 
  
  
  
}
// set methods 
-(void) setPosition:(CGPoint)pos;
-(void) setMapPosition_xx:(int)_xx _yy:(int)_yy;
-(void) setMoveSpeed:(float)duration;

// get methods 
-(int) getXX;
-(int) getYY;

-(CCSprite *) getEnemyObject;

// actions 
-(void) moveLeftOrRight_direction:(BOOL)dirRight; //이동방향으로 이동 
-(void) moveDown;

@end


#define ENEMY_SPRITE_FILE @"inv.png"