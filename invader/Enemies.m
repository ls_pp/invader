//
//  Enemies.m
//  invader
//
//  Created by Choe Yong-uk on 12. 2. 14..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "Enemies.h"
#import "enemy.h"

@implementation Enemies

@synthesize thisLayer;

-(id) init {	
  if( (self=[super init])) {
    // enemies 배열 초기화 
    enemies = [[NSMutableArray alloc] init];
    directionRight = YES; // 오른쪽으로 먼저 움직임 
    moveSpeed = 0.5f;
  }
  return self;
}

-(void) putEnemyAt_X:(int)xx _Y:(int)yy {
  // 가장 왼쪽 위 ccp좌표로는 (25/2, 480 - 25/2)
  // xx 가 증가하면 25/2 + (32 * xx) ==>>
  // yy 가 증가하면 480 - 25/2 + (32 * yy) \/ \/ 
  Enemy *e = [[Enemy alloc] init];
  [e setPosition:ccp(((25/2) + (32 * xx)), ((480 - 25/2) - (32 * yy)))];
  [e setMapPosition_xx:xx _yy:yy];
  [thisLayer addChild:[e getEnemyObject]];
  
  [enemies addObject:e];
  
}

-(void) putInitalizedEnemies {
  // 사실상 이게 초기화 
  // stage 1 
  // 가로 8개 인베이더
  // 세로 3줄 인베이더 
  // 속도 : ?? 
  for (int yy=0; yy<3; yy++)
  {
    for (int xx=0; xx<8; xx++) // 0~7
    {
      [self putEnemyAt_X:xx _Y:yy];
    }
  }
}

// 인베이더들이 벽에 닿았는지 점검 
-(BOOL) checkWall
{
  if (directionRight == YES) // 오른 쪽으로 가고 있었다면 
  {
    int max_xx = 0;

    for (Enemy *e in enemies)
    {
      if (max_xx < [e getXX]) max_xx = [e getXX];
    }
    //NSLog(@"max_xx:%d", max_xx);

    if (max_xx == 9) {
      //NSLog(@"touched rightWall");
      return YES; 
    }

  } else {
    // directionRight == NO
    int min_xx = 10;

    for (Enemy *e in enemies)
    {
      if (min_xx > [e getXX]) min_xx = [e getXX];
    }
    //NSLog(@"min_xx:%d", min_xx);

    if (min_xx == 0) {           
      //NSLog(@"touched leftWall");
      return YES; 
    }
  }
  return NO;
}

-(void) startMoveEnemies {
  id ac = [CCSequence actions:[CCCallFunc actionWithTarget:self selector:@selector(doMoveEnemies)], 
           [CCDelayTime actionWithDuration:(moveSpeed * 3)], 
           nil];
  [thisLayer runAction:[CCRepeatForever actionWithAction:ac]];
  //[thisLayer runAction:ac];
}

-(void) doMoveEnemies {
  // 배열 xx의 최대값/최소값 을 구해서 벽에 닿았는지 확인 
  if ([self checkWall] == YES)
  {
    moveSpeed = moveSpeed * 0.9f;  // 10% 감소 
    for (Enemy *e in enemies)
    {      
      [e moveDown]; // 아래로 한칸 내려가고
      [e setMoveSpeed:moveSpeed]; // 각각의 스프라이트에 set 
    }
    directionRight = ! directionRight;
  } else {
    
    // 아니라면 움직일것 각각이 모두 한꺼번에.. 가능할까 
    for (Enemy *e in enemies)
    {
      // getXX, getYY 사용법 : NSLog(@"각각의 xx값은? %d", [e getXX]);
      [e moveLeftOrRight_direction:directionRight];
    }
  }
}

-(BOOL) collusionWithMisslePosition:(CGPoint)mposition
{
  for (Enemy *e in enemies)
  {
    if (CGRectContainsPoint([[e getEnemyObject] boundingBox], mposition)) {
      //NSLog(@"collusion!! [%d][%d]", [e getXX], [e getYY]);

      CCSprite *bang = [CCSprite spriteWithFile:@"bang.png"];
      bang.position = [e getEnemyObject].position;
      
      [thisLayer removeChild:[e getEnemyObject] cleanup:YES];
      [enemies removeObject:e];
      
      id showanddestroy = [CCSequence actions:[CCDelayTime actionWithDuration:moveSpeed], 
                           [CCCallFuncN actionWithTarget:self selector:@selector(removeBang:)],
                           nil];
      [thisLayer addChild:bang];
      [bang runAction:showanddestroy];
      return YES;
    }
  }
  return NO;
}

-(void) removeBang:(id)sender
{
  [thisLayer removeChild:sender cleanup:YES];
}

-(int) getEnemyCount
{
  return [enemies count];
}
-(BOOL) isUnderBumperLine
{
  for (Enemy *e in enemies)
  {
    //NSLog(@"now [e getYY]:%d", [e getYY]);
    if ([e getYY] > 1) return YES;
    //if ([e getYY] > 11) return YES;
  }
  return NO;
}

@end
