//
//  Enemies.h
//  invader
//
//  Created by Choe Yong-uk on 12. 2. 14..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Enemies : NSObject
{
  BOOL directionRight; // right == YES / left == NO
  NSMutableArray *enemies;
  CCLayer *thisLayer;
  float moveSpeed;
}

@property (assign) CCLayer *thisLayer;

-(id)init;
// -(id) initOnLayer:(CCLayer *)ll; // 이렇게 하고 싶은데 ... 
-(int) getEnemyCount;
-(BOOL) isUnderBumperLine;

-(void) putEnemyAt_X:(int)xx _Y:(int)yy;
-(void) putInitalizedEnemies;
-(void) startMoveEnemies;
-(void) doMoveEnemies;
-(BOOL) collusionWithMisslePosition:(CGPoint)mposition;
-(void) removeBang:(id)sender;
@end
