//
//  enemy.m
//  invader
//
//  Created by Choe Yong-uk on 12. 2. 14..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "enemy.h"

@implementation Enemy


- (id)init {
	
  if( (self=[super init])) {
    sprite = [CCSprite spriteWithFile:ENEMY_SPRITE_FILE];
    
    // 변수 초기화 
    position = ccp(100,100); // 솔직히 대강 아무데나 
    xx = -1;
    yy = -1;
    moveSpeed = 0.5f;
    
  }
  return self;
}
-(void) setMoveSpeed:(float)duration {
  moveSpeed = duration;
  //NSLog(@"now moveDuration:%f", moveSpeed);
}

-(void) setPosition:(CGPoint)pos {
  sprite.position = pos;
  //NSLog(@"sp.position=(%.0f, %0.f) vs (%.0f, %.0f)", sprite.position.x, sprite.position.y, pos.x, pos.y);
}

-(void) setMapPosition_xx:(int)_xx _yy:(int)_yy {
  xx = _xx;
  yy = _yy;
  //NSLog(@"_xx, _yy = %d, %d", _xx, _yy);
}

-(CCSprite *) getEnemyObject {
  return sprite;
}

-(int) getXX {
  return xx;
}
-(int) getYY {
  return yy;
}

-(void) moveLeftOrRight_direction:(BOOL)dirRight
{
  if (dirRight == YES)
  {
    id goRight = [CCEaseIn actionWithAction:[CCMoveBy actionWithDuration:moveSpeed position:ccp(32, 0)] rate:1.0f];    
    [sprite runAction:goRight];
    xx++;
  } else {
    // left
    id goRight = [CCEaseIn actionWithAction:[CCMoveBy actionWithDuration:moveSpeed position:ccp(-32, 0)] rate:1.0f];    
    [sprite runAction:goRight];
    xx--;
  }
}

-(void) moveDown {
  id goDown = [CCEaseIn actionWithAction:[CCMoveBy actionWithDuration:moveSpeed position:ccp(0, -32)] rate:1.0f];    
  [sprite runAction:goDown];
  yy++;
}

@end
