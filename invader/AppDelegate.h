//
//  AppDelegate.h
//  invader
//
//  Created by Choe Yong-uk on 11. 7. 30..
//  Copyright none 2011년. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
