//
//  HelloWorldLayer.h
//  invader
//
//  Created by Choe Yong-uk on 11. 7. 30..
//  Copyright noizze.net 2011년. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Enemies.h"
#import "Bumper.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer
{
  BOOL onMoving;
  CCSprite *playerSprite;
  CCSprite *playerMissle;
  
  Enemies *ee;
  NSMutableArray *Bumpers;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

-(void) moveLeftCheck:(id)anObject;
-(void) moveRightCheck:(id)anObject;

-(void) shootMissle; // 쏘기 
-(void) checkMissleAndEnemies:(id)sender; // 충돌 확인 
-(void) checkMissleAndBumper:(id)sender; // 충돌 확인 

-(BOOL) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
-(BOOL) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
-(BOOL) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;

@end
