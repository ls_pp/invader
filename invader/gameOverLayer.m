//
//  gameOverLayer.m
//  invader
//
//  Created by Choe Yong-uk on 12. 2. 15..
//  Copyright 2012년 noizze.net. All rights reserved.
//

#import "gameOverLayer.h"
#import "HelloWorldLayer.h"

@implementation gameOverLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	gameOverLayer *layer = [gameOverLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
    self.isTouchEnabled = YES; // 글로벌 터치 인식 		

    CCLabelTTF *score_label = [CCLabelTTF labelWithString:@"GAME OVER"
                                               dimensions:CGSizeMake(320, 100) // width, height 
                                                alignment:CCTextAlignmentCenter
                                                 fontName:@"Helvetica-Bold" 
                                                 fontSize:24];
    score_label.position = ccp(320/2, 480/2);
    [self addChild:score_label z:150 tag:300];
  
    id s = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f], 
            [CCCallFunc actionWithTarget:self selector:@selector(drawButton)],
            nil];
            
    [self runAction:s];
  }
  return self;
}

-(void) drawButton
{
  
  CCLabelTTF *continue_label = [CCLabelTTF labelWithString:@"Tab Screen to Continue"
                                             dimensions:CGSizeMake(320, 100) // width, height 
                                              alignment:CCTextAlignmentCenter
                                               fontName:@"Helvetica" 
                                               fontSize:16];
  continue_label.position = ccp(320/2, 180);
  [self addChild:continue_label z:150 tag:301];
  
}
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  UITouch *touch = [touches anyObject];
  
  if (touch)
  {
    // 원래는 메뉴 화면으로 가야 하는데 
    [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInL transitionWithDuration:0.5f scene:[HelloWorldLayer scene]]];
  }
}


@end
