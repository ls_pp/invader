//
//  Bumper.m
//  invader
//
//  Created by Choe Yong-uk on 12. 2. 15..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import "Bumper.h"

@implementation Bumper

-(id)initWithPosition:(CGPoint)position _layer:(CCLayer *)thisLayer
{
  if( (self=[super init])) {
    /*
     [][][][] : 0
     [][][][] : 1
     범퍼는 이렇게 생겼음 각 블록은 20*20 
     가운데가 포지션앵커니까, 
     [0][0]의 포지션은 ccp(position.x-20-10, position.y+10)
     xx(가로)가 증가 할 수록 : (position.x-20-10 + (20 * xx))
     마찬가지로 
     yy(세로가 증가 하면 (아래쪽으로) : (position.y + 10 - (20 * yy))
    */
    sprites = [[NSMutableArray alloc] init];
    
    for (int yy=0; yy<=1; yy++)
    {
      for (int xx=0; xx<=3; xx++) 
      {
        CCSprite *b = [CCSprite spriteWithFile:@"damage0.png"];
        b.position = ccp(position.x -20 -10 +(20*xx), (position.y + 10 - (20 * yy)));
        NSMutableArray *sprUnit = [NSMutableArray arrayWithObjects:b, [NSNumber numberWithInt:0], nil];
        
        [sprites addObject:sprUnit];
        [thisLayer addChild:b];
      }
    }
    
    
  }
  return self;

}

-(BOOL) collusionWithMisslePosition:(CGPoint)mposition onLayer:(CCLayer *)thisLayer
{
  for (NSMutableArray *sprUnit in sprites)
  {
    CCSprite *b = [sprUnit objectAtIndex:0];
    if (CGRectContainsPoint([b boundingBox], mposition))
    {
      NSLog(@"coll with Bumper");
      if ([[sprUnit objectAtIndex:1] intValue] == 1) // damage 횟수가 1
      {
        [sprites removeObject:sprUnit];
        [thisLayer removeChild:b cleanup:YES];
        
      } else {
        CCSprite *new_b = [CCSprite spriteWithFile:@"damage1.png"];
        new_b.position = b.position;
        
        NSMutableArray *newUnit = [NSMutableArray arrayWithObjects:new_b, [NSNumber numberWithInt:1], nil];
        [sprites removeObject:sprUnit];
        [sprites addObject:newUnit];
        [thisLayer removeChild:b cleanup:YES];
        [thisLayer addChild:new_b];
      }
      return YES;
    }
  }
  return NO;
}

@end
