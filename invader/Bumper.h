//
//  Bumper.h
//  invader
//
//  Created by Choe Yong-uk on 12. 2. 15..
//  Copyright (c) 2012년 noizze.net. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface Bumper : NSObject {
  
  int damage[4][2];
  
  NSMutableArray *sprites;
  
}

-(id) initWithPosition:(CGPoint)position _layer:(CCLayer *)thisLayer;

-(BOOL) collusionWithMisslePosition:(CGPoint)mposition onLayer:(CCLayer *)thisLayer;


@end
