//
//  HelloWorldLayer.m
//  invader
//
//  Created by Choe Yong-uk on 11. 7. 30..
//  Copyright noizze.net 2011년. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "gameOverLayer.h"

// HelloWorldLayer implementation
@implementation HelloWorldLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
    self.isTouchEnabled = YES; // 글로벌 터치 인식 		
    onMoving = false;
    
    // init player
    playerSprite = [CCSprite spriteWithFile:@"imgres-1.jpeg"];
    playerSprite.position = ccp(320/2, 32);
    
		[self addChild: playerSprite];

    // init enemies
    ee = [[Enemies alloc] init];
    ee.thisLayer = self;
    [ee putInitalizedEnemies];
    
    
    // init bumpers 
    Bumpers = [[NSMutableArray alloc] init];
    Bumper *b1 = [[Bumper alloc] initWithPosition:ccp(100,80) _layer:self];
    Bumper *b2 = [[Bumper alloc] initWithPosition:ccp(220,80) _layer:self];
    [Bumpers addObject:b1];
    [Bumpers addObject:b2];
    
    
    // start
    UIAlertView *view=[[UIAlertView alloc] initWithTitle:@"READY?"
                                                 message:@""
                                                delegate:self cancelButtonTitle:nil
                                       otherButtonTitles:@"확 인", Nil];
    [view show];
    /*
    for (NSObject *o in [self children])
    {
      NSLog(@"%@", o);
    }
     */
	}
	return self;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  NSLog(@"buttonIndex:%d", buttonIndex);
  if (buttonIndex == 0)
  {
    [ee startMoveEnemies];
    id winOrLoseCheckAction = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f], 
                               [CCCallFunc actionWithTarget:self selector:@selector(checkWinOrLose)], 
                               nil];
    [self runAction:[CCRepeatForever actionWithAction:winOrLoseCheckAction]];
  }
}

-(void) checkWinOrLose
{
  // 승리 조건 : 모두 격파 
  if ([ee getEnemyCount] == 0)
  {
    [self stopAllActions];
    // 화면 전환 해서 점수 보여주도록   
    // 다음 스테이지로 ㄱㄱ
    return;
  }
  
  // 패배 조건 : 남은 아군포대가 없거나, 인베이더가 100 아래로 내려 왔을 때 
  if ([ee isUnderBumperLine] == YES)
  {
    [self stopAllActions];
    // 화면 전환 해서 펑펑 터지고 게임오버 
    // 점수 보여주고 메뉴화면으로 
    [[CCDirector sharedDirector] replaceScene:[CCTransitionMoveInR transitionWithDuration:0.5f scene:[gameOverLayer scene]]];
    return;
  }
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
   
-(void) moveLeftCheck:(id)anObject
{
  NSLog(@"moveLeftCheck");
  if (playerSprite.position.x < [playerSprite boundingBox].size.width/2 || onMoving == false)
  {
    // 화면 왼쪽 밖으로 나갔거나 터치를 뗐을 때 
    [playerSprite stopActionByTag:111];
    onMoving = false;
  }
}
-(void) moveRightCheck:(id)anObject
{
  NSLog(@"moveRightCheck");
  if (playerSprite.position.x > (320-[playerSprite boundingBox].size.width/2) || onMoving == false)
  {
    // 화면 오른쪽 밖으로 나갔거나 터치를 뗐을 때 
    [playerSprite stopActionByTag:222];
    onMoving = false;
  }
}

-(void) shootMissle
{
  // missle 객체를 만들어도 괜찮을 것 같은데 
  playerMissle = [CCSprite spriteWithFile:@"missle_up.png"];
  playerMissle.position = playerSprite.position;

  id shootup = [CCSequence actions:
                [CCMoveBy actionWithDuration:0.025f position:ccp(0, 16)],
                [CCCallFuncN actionWithTarget:self selector:@selector(checkMissleAndBumper:)], 
                [CCCallFuncN actionWithTarget:self selector:@selector(checkMissleAndEnemies:)], 
                nil];
  [self addChild:playerMissle];
  [playerMissle runAction:[CCRepeatForever actionWithAction:shootup]];
}

-(void) checkMissleAndBumper:(id)sender {
  CCSprite *m = sender;
  // Bumper와 충돌 판정 
  for (Bumper *b in Bumpers) 
  {
    if ([b collusionWithMisslePosition:m.position onLayer:self])
    {
      [self removeChild:m cleanup:YES];
    }
  }
  
}

-(void) checkMissleAndEnemies:(id)sender 
{
  CCSprite *m = sender;
  // 화면 넘어가거나 충돌하면 스프라이트 없애버림 
  if (m.position.y > 480)
  {
    [self removeChild:m cleanup:YES];
    return;
  }
  // Enemies 와 충돌 판정 
  if ([ee collusionWithMisslePosition:m.position])
  {
    // 
    [self removeChild:m cleanup:YES];
  }
}

// touch event Start ========== ========== ==========
-(BOOL) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
  UITouch *touch = [touches anyObject];
  
  if (touch)
  {
    CGPoint touchedlocation = [[CCDirector sharedDirector] convertToGL: [touch locationInView:touch.view]];
    //NSLog(@"touchEnded:ccp(%.0f, %.0f)", touchedlocation.x, touchedlocation.y);

    if (touchedlocation.x <= 320/3) // 화면의 왼쪽
    {
      //NSLog(@"left");
      onMoving = true;
      id moveLeftFunc = [CCCallFuncN actionWithTarget:self selector:@selector(moveLeftCheck:)];
      id moveLeftAction = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                             [CCMoveBy actionWithDuration:0.1f position:ccp(-16,0)],
                                                             moveLeftFunc, nil]];
      [moveLeftAction setTag:111];
      [playerSprite runAction:moveLeftAction];
    } else if (touchedlocation.x > 320/3 && touchedlocation.x <= 320/3*2) { // 화면 중앙부근 
      //NSLog(@"shoot");      
      [self shootMissle];
    } else if (touchedlocation.x > 320/3*2) { // 화면 오른쪽 
      //NSLog(@"right");    
      onMoving = true;
      id moveRightFunc = [CCCallFuncN actionWithTarget:self selector:@selector(moveRightCheck:)];
      id moveRightAction = [CCRepeatForever actionWithAction:[CCSequence actions:
                                                             [CCMoveBy actionWithDuration:0.1f position:ccp(16,0)],
                                                             moveRightFunc, nil]];
      [moveRightAction setTag:222];
      [playerSprite runAction:moveRightAction];
    }
    
  }
	return YES;
}

-(BOOL) ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
  // animation stop 
  onMoving = false;
  return YES;
}

-(BOOL) ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
  
	return YES;
}
// touch event finished ========== ========== ==========




@end
